let request = require('request');
let cheerio = require("cheerio");
const https = require('https');
const NodeGeocoder = require('node-geocoder');
const options = {
    provider: 'yandex'
};
const geocoder = NodeGeocoder(options);
//переносим в модуль для app.js
module.exports = function (url) {
    return new Promise(function (resolve, reject) {
        //заголовки для запроса
            var headers = {
                'User-Agent': 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)',
                'Content-Type': 'application/x-www-form-urlencoded'
            };
            //запрос к url из консоли
            request({url: url, headers: headers}, function (error, response, body) {
//проверка на ошибку
                if (!error) {
                    let adress = false;
                    const $ = cheerio.load(body);
                    let regex = /cian.ru/gi;

                    if (url.match(regex)) {
                        adress = $("address").text().replace(/На карте/g, "");
                    } else {
                        adress = $(".information__address___1ZM6d").eq(0).text();
                    }
                    geocoder.geocode(adress, function (err, res) {
                        var latitude = res[0]['latitude'];
                        var longitude = res[0]['longitude'];

                        let url = "https://nominatim.openstreetmap.org/reverse?format=jsonv2&lat=" + latitude + "&lon=" + longitude + "";
                        let opts = require('url').parse(url);
                        opts.headers = {
                            'User-Agent': 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)'
                        }
                        let req = https.get(opts, function (res) {
                            var body = ''
                            res.on('data', function (chunk) {
                                body += chunk;
                                body = JSON.parse(body);
                                console.log(body)
                                console.log(body['address']['state_district'])
                            });

                        })
                    });
                }
            })
        }
    )
}
